package com.student.click.util;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogManager {
    ProgressDialog dialog;
    public ProgressDialogManager(String title, String text, Context ctx){
        this.dialog = new ProgressDialog(ctx);
        this.dialog.setTitle(title);
        this.dialog.setMessage(text);
        this.dialog.show();
    }
    public void hideDialog(){
        this.dialog.dismiss();
    }

}
