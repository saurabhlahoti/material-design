package com.student.click.util;

import android.content.Context;
import android.os.Environment;

import com.student.click.database.DatabaseOperations;
import com.student.click.entities.Student;

import java.io.File;

public class DeleteData {
    public DeleteData() {
    }
    public void deleteStudent(Student student,  Context ctx) {
        String imageName = student.getImage();
        String roll=student.getRoll();
        File image = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "StudentMS" + File.separator + "SAVED" + File.separator + imageName + ".jpg");
        if(image.exists()) {
            image.delete();
        }
        DatabaseOperations dop=new DatabaseOperations(ctx);
        dop.deleteInfo(dop, roll);
        dop.close();
    }
}
