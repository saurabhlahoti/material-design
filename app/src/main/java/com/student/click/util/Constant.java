package com.student.click.util;

import android.os.Environment;

import java.io.File;

/**
 * Created by click on 6/29/15.
 */
public class Constant {
    public static final int TOKEN=100;
    public static final int TOKEN_IMAGE_FROM_CAMERA=4;
    public static final int TOKEN_IMAGE_FROM_GALLERY=5;
    public static final int VIEW_STUDENT_DETAIL=0;
    public static final int EDIT_STUDENT_DETAIL=1;
    public static final int DELETE_STUDENT_DETAIL=2;
    public static final int CAMERA_OPTION=0;
    public static final int GALLERY_OPTION=1;
    public static final int GET_DATA=0;
    public static final int DELETE_DATA=1;
    public static final int INSERT_DATA=2;
    public static final int UPDATE_DATA=3;
    public static final int ASYNC_TASK=0;
    public static final int SERVICE=1;
    public static final int INTENT_SERVICE=2;
    public static final String SERVICE_RESPONSE = "Response From Service";
    public static final String SERVICE_INTENT_RESPONSE = "Response From Service Intent";
    public static final String IMAGE_DIRECTORY = Environment.getExternalStorageDirectory() + File.separator + "StudentMS" + File.separator + "SAVED";



    public Constant(){

    }
}
