package com.student.click.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.student.click.entities.Student;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.ImageDisplay;


public class DisplayDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_details);

        ImageDisplay imageDisplay = new ImageDisplay();
        Intent receiver = getIntent();
        Student student = (Student) receiver.getSerializableExtra("viewField");
        TextView nameDetails = (TextView) findViewById(R.id.nameDetail);
        nameDetails.setText(student.getName());
        TextView branchDetails = (TextView) findViewById(R.id.branchDetail);
        branchDetails.setText(student.getBranch());
        TextView rollDetails = (TextView) findViewById(R.id.rollDetail);
        rollDetails.setText(student.getRoll());
        ImageView imageDetails = (ImageView) findViewById(R.id.imageDetail);
        String imageName = student.getImage() + ".jpg";

        if (imageName.compareTo("profile.jpg") == 0) {
            imageDetails.setImageResource(imageDisplay.profileImage());
        } else {
            imageDetails.setImageBitmap(imageDisplay.renderImage(imageName, this));
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_display_details,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.exit:
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
