package com.student.click.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.student.click.database.DatabaseOperations;
import com.student.click.entities.Student;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.Constant;
import com.student.click.util.ImageDisplay;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

;

public class StudentEntry extends AppCompatActivity {

    int id;
    ImageView profileImage;
    String name, oldRoll = "", roll, branch, image;
    ImageButton setImageToDefaultButton;
    ImageDisplay imageDisplay;
    Boolean flag=false;
    ArrayList<String> imageList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        image = "profile";  //initializing
        setContentView(R.layout.activity_student_entry);
        imageDisplay = new ImageDisplay();

        setImageToDefaultButton = (ImageButton) findViewById(R.id.setDefaultImageButton);
        setImageToDefaultButton.setVisibility(View.GONE);
        profileImage = (ImageView) findViewById(R.id.profileImage);
        id = -1;
        try {
            flag=true;
            Intent receiver = getIntent();
            Student studentEdit = (Student) receiver.getSerializableExtra("editField");
            id = receiver.getIntExtra("id", -1);
            EditText name = (EditText) findViewById(R.id.name);
            name.setText(studentEdit.getName());
            EditText branch = (EditText) findViewById(R.id.branch);
            branch.setText(studentEdit.getBranch());
            EditText roll = (EditText ) findViewById(R.id.roll);
            roll.setText(studentEdit.getRoll());
            roll.setFocusable(false);
            oldRoll = studentEdit.getRoll();
            image = studentEdit.getImage();
            String imageName = image + ".jpg";

            if (imageName.compareTo("profile.jpg") == 0) {
                profileImage.setImageResource(imageDisplay.profileImage());
            } else {
                profileImage.setImageBitmap(imageDisplay.renderImage(imageName, this));
                setImageToDefaultButton.setVisibility(View.VISIBLE);
            }
        } catch (NullPointerException e) {
            e.fillInStackTrace();
        }


        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        Button addStudentButton = (Button) findViewById(R.id.addStudentButton);


        addStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv;
                tv = (TextView) findViewById(R.id.name);
                name = tv.getText().toString();
                tv = (TextView) findViewById(R.id.roll);
                roll = tv.getText().toString();
                tv = (TextView) findViewById(R.id.branch);
                branch = tv.getText().toString();
                if(validate(name,roll,branch)){
                    DatabaseOperations databaseOperations = new DatabaseOperations(StudentEntry.this);
                    Cursor cursor = databaseOperations.getRecord(databaseOperations,roll);
                    if(!cursor.moveToFirst()||flag){
                        Student student = new Student(name, roll, branch, image);
                        Intent intent = new Intent();
                        //Bundle bundle=new Bundle();
                        // bundle.putSerializable("details",(Serializable)student);
                        // intent.putExtra("bundle", bundle);
                        intent.putExtra("details", student);
                        intent.putExtra("oldRoll", oldRoll);
                        intent.putExtra("id", id);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                    else{
                        EditText et=(EditText)findViewById(R.id.roll);
                        et.setError("Already Exists");
                    }
                }
            }
        });
    }
    Boolean validate(String name,String roll,String branch){
        EditText et;
        if(name.compareTo("")==0){
            et=(EditText)findViewById(R.id.name);
            et.setHovered(true);
            et.setError("Field Empty");
            return false;
        }
        if(branch.compareTo("")==0){
            et=(EditText)findViewById(R.id.branch);
            et.setHovered(true);
            et.setError("Field Empty");
            return false;
        }
        if(roll.compareTo("")==0){
            et=(EditText)findViewById(R.id.roll);
            et.setHovered(true);
            et.setError("Field Empty");
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_student_entry, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.exit:
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    ////
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("imageName", image);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop(); // deleting unused files
        for (int i = 0; i < imageList.size(); i++) {
            String tempFile = imageList.get(i);
            if (tempFile.compareTo(image) != 0) {
                try {
                    new File(Constant.IMAGE_DIRECTORY + File.separator + tempFile + ".jpg").delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // onRestoreInstanceState
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        image = (String) savedInstanceState.getString("imageName");
        String imageName = image + ".jpg";
        if (imageName.compareTo("profile.jpg") == 0) {
            profileImage.setImageResource(imageDisplay.profileImage());
        } else {
            profileImage.setImageBitmap(imageDisplay.renderImage(imageName, this));
            setImageToDefaultButton.setVisibility(View.VISIBLE);
        }
    }


    void selectImage() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Add Profile Image");
        dialog.setItems(R.array.image_menu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == Constant.CAMERA_OPTION) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent, Constant.TOKEN_IMAGE_FROM_CAMERA);
                } else if (item == Constant.GALLERY_OPTION) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, Constant.TOKEN_IMAGE_FROM_GALLERY);
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setImageToDefaultButton.setVisibility(View.VISIBLE);
            if (requestCode == Constant.TOKEN_IMAGE_FROM_CAMERA) {
                File tempCamFile = new File(Environment.getExternalStorageDirectory() + "/temp.jpg");
                try {
                    Bitmap bitmap;//=MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(file));
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(tempCamFile.getAbsolutePath(), bitmapOptions);
                    profileImage.setImageBitmap(bitmap);

                    File myDir = new File(Constant.IMAGE_DIRECTORY);
                    myDir.mkdirs();
                    OutputStream outFile = null;
                    image = String.valueOf(System.currentTimeMillis());
                    File finalFile = new File(myDir.getAbsolutePath(), image + ".jpg");
                    tempCamFile.delete();
                    try {
                        outFile = new FileOutputStream(finalFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == Constant.TOKEN_IMAGE_FROM_GALLERY) {
                Uri selectedImage = data.getData();
                try {

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    profileImage.setImageBitmap(bitmap);
                    File myDir = new File(Constant.IMAGE_DIRECTORY);
                    try {
                        myDir.mkdirs();
                    } catch (Exception e) {
                        Log.e("DEBUG", "Could not write file " + e.getMessage());
                    }
                    image = String.valueOf(System.currentTimeMillis());
                    File finalFile = new File(myDir.getAbsolutePath(), image + ".jpg");
                    try {
                        FileOutputStream out = new FileOutputStream(finalFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);
                        out.flush();
                        out.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            imageList.add(image);
        }
    }

    public void setImageToDefault(View view) {
        image = "profile";
        profileImage.setImageResource(imageDisplay.profileImage());
        setImageToDefaultButton.setVisibility(View.GONE);
    }


}
