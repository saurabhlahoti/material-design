package com.student.click.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.student.click.database.TableData.TableInfo;
import com.student.click.entities.Student;

public class DatabaseOperations extends SQLiteOpenHelper {
    public static int databaseVersion = 1;
    public String createQuery = "create table " + TableInfo.TABLE_NAME + " (" + TableInfo.STUDENT_NAME + " text, " +
            TableInfo.STUDENT_ROLL + " integer, " + TableInfo.STUDENT_BRANCH + " text, "
            + TableInfo.STUDENT_IMAGE + " text);";

    public DatabaseOperations(Context context) {
        super(context, TableInfo.DATABASE_NAME, null, databaseVersion);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createQuery);
        Log.d("database", "successfully table created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void putInfo(DatabaseOperations dop, Student student) {
        String name = student.getName();
        String roll = student.getRoll();
        String branch = student.getBranch();
        String image = student.getImage();
        SQLiteDatabase db = dop.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableInfo.STUDENT_NAME, name);
        cv.put(TableInfo.STUDENT_ROLL, roll);
        cv.put(TableInfo.STUDENT_BRANCH, branch);
        cv.put(TableInfo.STUDENT_IMAGE, image);
        db.insert(TableInfo.TABLE_NAME, null, cv);
    }

    public Cursor getInfo(DatabaseOperations dop) {
        SQLiteDatabase db = dop.getReadableDatabase();
        String[] tableColumns = {TableInfo.STUDENT_NAME, TableInfo.STUDENT_ROLL,
                TableInfo.STUDENT_BRANCH, TableInfo.STUDENT_IMAGE};
        Cursor cursor = db.query(TableInfo.TABLE_NAME, tableColumns, null, null, null, null, null);
        return cursor;
    }

    public Cursor getRecord(DatabaseOperations dop, String roll) {
        SQLiteDatabase db = dop.getReadableDatabase();
        String[] tableColumns = {TableInfo.STUDENT_NAME};
        String whereClause = TableInfo.STUDENT_ROLL + " LIKE ? ";
        String [] whereArgs = { roll };
        Cursor cursor = db.query(TableInfo.TABLE_NAME, tableColumns,whereClause,whereArgs,null,null,null);
        return cursor;
    }

    public void deleteInfo(DatabaseOperations dop, String roll) {
        String whereClause = TableInfo.STUDENT_ROLL + " LIKE ?";
        String[] args = {roll};
        SQLiteDatabase db = dop.getWritableDatabase();
        db.delete(TableInfo.TABLE_NAME, whereClause, args);
    }
    /*
    Getting specific row from database
     public Cursor getInfo(DatabaseOperations dop) {
        SQLiteDatabase db = dop.getReadableDatabase();
        String whereClause = TableInfo.STUDENT_NAME + " LIKE ? "
        String []  columsThatWeWant = { TableInfo.UserPass };
        String [] whereClauseArguments = { user };
        Cursor cursor = db.query ( <tablename>, columns , whereClause ,  whereClauseArguments , null , null , null );
     */


    /*Updating specific row in database*/
    public void updateInfo(DatabaseOperations dop, String old_roll, String name, String roll, String branch, String image) {
        SQLiteDatabase db = dop.getReadableDatabase();
        String whereClause = TableInfo.STUDENT_ROLL + " LIKE ? ";
        String[] whereClauseArguments = {old_roll};
        ContentValues values = new ContentValues();
        values.put(TableInfo.STUDENT_NAME, name);
        values.put(TableInfo.STUDENT_BRANCH, branch);
        values.put(TableInfo.STUDENT_ROLL, roll);
        values.put(TableInfo.STUDENT_IMAGE, image);
        db.update(TableInfo.TABLE_NAME, values, whereClause, whereClauseArguments);
    }
}
