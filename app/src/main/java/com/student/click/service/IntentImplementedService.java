package com.student.click.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;

import com.student.click.database.DatabaseOperations;
import com.student.click.entities.Student;
import com.student.click.util.Constant;
import com.student.click.util.DeleteData;

import java.util.ArrayList;

public class IntentImplementedService extends IntentService {


    public IntentImplementedService(String name) {
        super(name);
    }

    public IntentImplementedService() {
        super("Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            int action = intent.getIntExtra("action", -1);
            ArrayList<Student> studentList;
            Student student;
            Context context = getApplicationContext();
            DatabaseOperations dbOperation = new DatabaseOperations(context);
            Intent broadcast = new Intent();
            broadcast.setAction(Constant.SERVICE_INTENT_RESPONSE);
            broadcast.addCategory(Intent.CATEGORY_DEFAULT);
            switch (action) {
                case Constant.GET_DATA:
                    studentList = (ArrayList<Student>) intent.getSerializableExtra("studentList");
                    Cursor cursor = dbOperation.getInfo(dbOperation);
                    cursor.moveToFirst();
                    try {
                        do {
                            Student obj = new Student(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                            studentList.add(obj);
                        } while (cursor.moveToNext());
                    } catch (CursorIndexOutOfBoundsException e) {
                        e.fillInStackTrace();
                    }
                    broadcast.putExtra("studentList", studentList);
                    break;
                case Constant.DELETE_DATA:
                    student = (Student) intent.getSerializableExtra("student");
                    new DeleteData().deleteStudent(student, context);
                    break;
                case Constant.UPDATE_DATA:
                    student = (Student) intent.getSerializableExtra("student");
                    String oldRoll = intent.getStringExtra("oldRoll");
                    String name = student.getName();
                    String branch = student.getBranch();
                    String roll = student.getRoll();
                    String image = student.getImage();
                    dbOperation.updateInfo(dbOperation, oldRoll, name, roll, branch, image);
                    break;
                case Constant.INSERT_DATA:
                    student = (Student) intent.getSerializableExtra("student");
                    dbOperation.putInfo(dbOperation, student);
                    break;
                default:
            }
            dbOperation.close();
            sendBroadcast(broadcast);
        } catch (NullPointerException e) {
            stopService(intent);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
