package com.student.click.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.student.click.database.DatabaseOperations;
import com.student.click.entities.Student;
import com.student.click.service.DatabaseService;
import com.student.click.service.IntentImplementedService;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.Constant;
import com.student.click.util.DeleteData;
import com.student.click.util.GridViewAdapter;
import com.student.click.util.ListViewAdapter;
import com.student.click.util.ProgressDialogManager;
import com.student.click.util.Sort;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Student> studentList = new ArrayList<Student>();
    ListView list;
    ListViewAdapter listAdapter;
    GridView grid;
    String view = "list";
    String sort = "alpha";
    int resIdView, resIdSort, taskSelection;
    GridViewAdapter gridAdapter;
    static ImageButton toggleView, toggleSort;
    ServiceReceiver serviceReceiver;
    ServiceIntentReceiver serviceIntentReceiver;


    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        toolbar = (Toolbar)findViewById(R.id.activityBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        toggleView = (ImageButton) findViewById(R.id.viewImageButton);
        toggleSort = (ImageButton) findViewById(R.id.sortImageButton);
        resIdView = R.mipmap.grid;
        resIdSort = R.mipmap.sortname;
        toggleView.setImageResource(resIdView);
        toggleSort.setImageResource(resIdSort);
        list = (ListView) findViewById(R.id.studentList);
        grid = (GridView) findViewById(R.id.studentGrid);
        listAdapter = new ListViewAdapter(this, studentList);
        gridAdapter = new GridViewAdapter(this, studentList);



        list.setAdapter(listAdapter);
        grid.setAdapter(gridAdapter);
        grid.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            view = savedInstanceState.getString("view");
            taskSelection = savedInstanceState.getInt("taskSelection");
            int viewButton = savedInstanceState.getInt("viewButton");
            int sortButton = savedInstanceState.getInt("sortButton");
            toggleView.setImageResource(viewButton);
            toggleSort.setImageResource(sortButton);
            if (view.compareTo("grid") == 0) {
                grid.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
            } else {
                grid.setVisibility(View.GONE);
                list.setVisibility(View.VISIBLE);
            }
        }

        IntentFilter serviceFilter = new IntentFilter(Constant.SERVICE_RESPONSE);
        serviceFilter.addCategory(Intent.CATEGORY_DEFAULT);
        serviceReceiver = new ServiceReceiver();
        registerReceiver(serviceReceiver, serviceFilter);

        IntentFilter serviceIntentFilter = new IntentFilter(Constant.SERVICE_INTENT_RESPONSE);
        serviceIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        serviceIntentReceiver = new ServiceIntentReceiver();
        registerReceiver(serviceIntentReceiver, serviceIntentFilter);

        Intent intent = getIntent();

        taskSelection = intent.getIntExtra("task", -1);
        switch (taskSelection) {
            case Constant.ASYNC_TASK:
                new AsyncDataRetrieval().execute(this);
                break;
            case Constant.SERVICE:
                Intent databaseService = new Intent(this, DatabaseService.class);
                databaseService.putExtra("action", Constant.GET_DATA);
                databaseService.putExtra("studentList", studentList);
                startService(databaseService);
                break;
            case Constant.INTENT_SERVICE:
                Intent intentService = new Intent(this, IntentImplementedService.class);
                intentService.putExtra("action", Constant.GET_DATA);
                intentService.putExtra("studentList", studentList);
                startService(intentService);
                break;
            default:
        }


        toggleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialogManager manager;
                manager = new ProgressDialogManager("", "Please wait... ", MainActivity.this);
                if (resIdView == R.mipmap.grid) {
                    resIdView = R.mipmap.list;
                    view = "grid";
                    list.setVisibility(View.GONE);
                    grid.setVisibility(View.VISIBLE);
                } else {
                    resIdView = R.mipmap.grid;
                    view = "list";
                    grid.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                }
                toggleView.setImageResource(resIdView);
                manager.hideDialog();
            }

        });

        toggleSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sort sortClass = new Sort();
                if (resIdSort == R.mipmap.sortname) {
                    resIdSort = R.mipmap.sortroll;
                    sort = "num";
                    studentList = sortClass.sortByName(studentList);
                } else {
                    sort = "alpha";
                    resIdSort = R.mipmap.sortname;
                    studentList = sortClass.sortByRoll(studentList);
                }
                try {
                    toggleSort.setImageResource(resIdSort);
                } catch (NullPointerException e) {
                    e.fillInStackTrace();
                }
                listAdapter.notifyDataSetChanged();
                gridAdapter.notifyDataSetChanged();
            }
        });
        registerForContextMenu(list);
        registerForContextMenu(grid);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.TOKEN) {
            //bundle=data.getExtras();
            try {
                Student studentNow = (Student) data.getSerializableExtra("details");         //using without (Serializable)<object> in putextra() method in source activity
                //Student studentNow = (Student) data.getExtras().getSerializable("details"); using with (Serializable)<object> in putextra() method in source activity
                studentList = listAdapter.getStudentList();
                //UPDATING RECORD
                int id = data.getIntExtra("id", -1);
                if (id != -1) {
                    String oldRoll = data.getStringExtra("oldRoll");
                    switch (taskSelection) {
                        case Constant.ASYNC_TASK:
                            new AsyncDataUpdation().execute(id, oldRoll, studentNow, MainActivity.this);
                            break;
                        case Constant.SERVICE:
                            Intent databaseService = new Intent(this, DatabaseService.class);
                            databaseService.putExtra("action", Constant.UPDATE_DATA);
                            studentList.remove(id);
                            studentList.add(studentNow);
                            databaseService.putExtra("student", studentNow);
                            databaseService.putExtra("oldRoll", oldRoll);
                            startService(databaseService);
                            break;
                        case Constant.INTENT_SERVICE:
                            Intent intentService = new Intent(this, IntentImplementedService.class);
                            intentService.putExtra("action", Constant.UPDATE_DATA);
                            studentList.remove(id);
                            studentList.add(studentNow);
                            intentService.putExtra("student", studentNow);
                            intentService.putExtra("oldRoll", oldRoll);
                            startService(intentService);
                            break;
                        default:
                    }
                    listAdapter.setStudentList(studentList);
                    gridAdapter.setStudentList(studentList);
                    listAdapter.notifyDataSetChanged();
                    gridAdapter.notifyDataSetChanged();


                    Toast.makeText(this, "List Updated", Toast.LENGTH_SHORT).show();
                }


                //NEW STUDENT ADDITION OR ADDING UPDATED RECORD
                if (studentNow.getBranch().compareTo("") != 0 && studentNow.getName().compareTo("") != 0 && studentNow.getRoll().compareTo("") != 0 && id == -1) {
                    studentList = listAdapter.getStudentList();
                    switch (taskSelection) {
                        case Constant.ASYNC_TASK:
                            new AsyncDataInsertion().execute(studentNow);
                            break;
                        case Constant.SERVICE:
                            Intent databaseService = new Intent(this, DatabaseService.class);
                            databaseService.putExtra("action", Constant.INSERT_DATA);
                            studentList.add(studentNow);
                            databaseService.putExtra("student", studentNow);
                            startService(databaseService);
                            break;
                        case Constant.INTENT_SERVICE:
                            Intent intentService = new Intent(this, IntentImplementedService.class);
                            intentService.putExtra("action", Constant.INSERT_DATA);
                            studentList.add(studentNow);
                            intentService.putExtra("student", studentNow);
                            startService(intentService);
                            break;
                        default:
                    }
                    listAdapter.setStudentList(studentList);
                    gridAdapter.setStudentList(studentList);
                    listAdapter.notifyDataSetChanged();
                    gridAdapter.notifyDataSetChanged();

                    Toast.makeText(this, "Student Added", Toast.LENGTH_SHORT).show();
                }

            } catch (NullPointerException e) {
                e.fillInStackTrace();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter serviceFilter = new IntentFilter(Constant.SERVICE_RESPONSE);
        serviceFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(serviceReceiver, serviceFilter);
        IntentFilter serviceIntentFilter = new IntentFilter(Constant.SERVICE_INTENT_RESPONSE);
        serviceIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(serviceIntentReceiver, serviceIntentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(serviceIntentReceiver);
            unregisterReceiver(serviceReceiver);
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }

    }
    @Override
    protected void onDestroy() {
        list.setAdapter(null);
        grid.setAdapter(null);
        super.onDestroy();
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("taskSelection", taskSelection);
        try {
            unregisterReceiver(serviceIntentReceiver);
            unregisterReceiver(serviceReceiver);
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        int sortButton = 0;
        if (sort.compareTo("alpha") == 0) {
            sortButton = R.mipmap.sortname;
        } else {
            sortButton = R.mipmap.sortroll;
        }
        int viewButton = 0;
        if (view.compareTo("grid") == 0)
            viewButton = R.mipmap.list;
        if (view.compareTo("list") == 0)
            viewButton = R.mipmap.grid;
        savedInstanceState.putString("view", view);
        //savedInstanceState.putString("sort", sort);
        savedInstanceState.putInt("viewButton", viewButton);
        savedInstanceState.putInt("sortButton", sortButton);

        Log.d("rotation", view + viewButton + " " + sortButton);
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        if (view.getId() == R.id.studentList || view.getId() == R.id.studentGrid) {
            //AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            String[] menuItems = getResources().getStringArray(R.array.menu);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //RVContextMenu.RecyclerContextMenuInfo info = (RVContextMenu.RecyclerContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        studentList = listAdapter.getStudentList();
        Student student = studentList.get(info.position);
        Intent intent;
        switch (menuItemIndex) {
            case Constant.VIEW_STUDENT_DETAIL:
                intent = new Intent(MainActivity.this, DisplayDetails.class);
                intent.putExtra("viewField", student);
                startActivity(intent);
                break;
            case Constant.EDIT_STUDENT_DETAIL:
                intent = new Intent(MainActivity.this, StudentEntry.class);
                intent.putExtra("editField", student);
                intent.putExtra("id", info.position);
                startActivityForResult(intent, Constant.TOKEN);
                break;
            case Constant.DELETE_STUDENT_DETAIL:
                switch (taskSelection) {
                    case Constant.ASYNC_TASK:
                        new AsyncDataDeletion().execute(info.position);
                        break;
                    case Constant.SERVICE:
                        Intent databaseService = new Intent(this, DatabaseService.class);
                        databaseService.putExtra("action", Constant.DELETE_DATA);
                        databaseService.putExtra("student", studentList.get(info.position));
                        studentList.remove(info.position);
                        startService(databaseService);
                        break;
                    case Constant.INTENT_SERVICE:
                        Intent intentService = new Intent(this, IntentImplementedService.class);
                        intentService.putExtra("action", Constant.DELETE_DATA);
                        intentService.putExtra("student", studentList.get(info.position));
                        studentList.remove(info.position);
                        startService(intentService);
                        break;
                    default:
                }
                Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
            default:
        }
        listAdapter.setStudentList(studentList);
        gridAdapter.setStudentList(studentList);
        listAdapter.notifyDataSetChanged();
        gridAdapter.notifyDataSetChanged();

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.goToStudentEntryFormButton:
                Intent intent = new Intent(MainActivity.this, StudentEntry.class);
                startActivityForResult(intent, Constant.TOKEN);
                break;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                //or onBackPressed(); and remove changes in MainActivity Meta Data
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    public class ServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == Constant.SERVICE_RESPONSE) {
                try {
                    studentList = (ArrayList<Student>) intent.getSerializableExtra("studentList");
                    if (studentList != null) {
                        listAdapter.setStudentList(studentList);
                        gridAdapter.setStudentList(studentList);
                        listAdapter.notifyDataSetChanged();
                        gridAdapter.notifyDataSetChanged();

                    }
                    stopService(new Intent(MainActivity.this, DatabaseService.class));

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public class ServiceIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == Constant.SERVICE_INTENT_RESPONSE) {
                try {
                    studentList = (ArrayList<Student>) intent.getSerializableExtra("studentList");
                    if (studentList != null) {
                        listAdapter.setStudentList(studentList);
                        gridAdapter.setStudentList(studentList);
                        listAdapter.notifyDataSetChanged();
                        gridAdapter.notifyDataSetChanged();

                    }
                    stopService(new Intent(MainActivity.this, IntentImplementedService.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }
    }


    public class AsyncDataRetrieval extends AsyncTask<Context, Void, ArrayList<Student>> {
        Context context;

        //private final ProgressDialog dialog =new ProgressDialog(MainActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // dialog.show(MainActivity.this, "Loading Student Data", "Please Wait...");
        }

        @Override
        protected ArrayList<Student> doInBackground(Context... ctx) {
            context = ctx[0];
            DatabaseOperations dbOperation = new DatabaseOperations(ctx[0]);
            Cursor cursor = dbOperation.getInfo(dbOperation);
            cursor.moveToFirst();
            try {
                do {
                    Student obj = new Student(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                    studentList.add(obj);
                } while (cursor.moveToNext());
            } catch (CursorIndexOutOfBoundsException e) {
                e.fillInStackTrace();
            }
            dbOperation.close();
            return studentList;
        }

        @Override
        protected void onPostExecute(ArrayList<Student> list) {
            super.onPostExecute(list);
            listAdapter.notifyDataSetChanged();
            gridAdapter.notifyDataSetChanged();

        }
    }

    public class AsyncDataUpdation extends AsyncTask<Object, Void, ArrayList<Student>> {

        //private final ProgressDialog dialog =new ProgressDialog(MainActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // dialog.show(MainActivity.this, "Loading Student Data", "Please Wait...");
        }

        @Override
        protected ArrayList<Student> doInBackground(Object... obj) {
            DatabaseOperations dbOperation = new DatabaseOperations((Context) obj[3]);
            int id = (int) obj[0];
            String oldRoll = (String) obj[1];
            Student studentUpdate = (Student) obj[2];
            studentList.remove(id);
            studentList.add(studentUpdate);
            String name = studentUpdate.getName();
            String branch = studentUpdate.getBranch();
            String roll = studentUpdate.getRoll();
            String image = studentUpdate.getImage();
            dbOperation.updateInfo(dbOperation, oldRoll, name, roll, branch, image);
            dbOperation.close();
            return studentList;
        }

        @Override
        protected void onPostExecute(ArrayList<Student> list) {
            super.onPostExecute(list);
            listAdapter.notifyDataSetChanged();
            gridAdapter.notifyDataSetChanged();

        }
    }

    public class AsyncDataDeletion extends AsyncTask<Integer, Void, ArrayList<Student>> {
        //ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialog.show(MainActivity.this, "Deleting Student Data", "Please Wait...");
        }

        @Override
        protected ArrayList<Student> doInBackground(Integer... id) {
            Student deleteStudentObj = studentList.get(id[0]);
            studentList.remove(id[0]);
            new DeleteData().deleteStudent(deleteStudentObj, MainActivity.this);
            return studentList;
        }

        @Override
        protected void onPostExecute(ArrayList<Student> students) {
            super.onPostExecute(students);
            //dialog.dismiss();
            listAdapter.notifyDataSetChanged();
            gridAdapter.notifyDataSetChanged();

        }
    }

    public class AsyncDataInsertion extends AsyncTask<Student, Void, ArrayList<Student>> {
        //ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialog.show(MainActivity.this, "Inserting Student Data", "Please Wait...");
        }

        @Override
        protected ArrayList<Student> doInBackground(Student... student) {
            studentList.add(student[0]);
            DatabaseOperations dbOperation = new DatabaseOperations(MainActivity.this);
            dbOperation.putInfo(dbOperation, student[0]);
            dbOperation.close();
            return studentList;
        }

        @Override
        protected void onPostExecute(ArrayList<Student> students) {
            super.onPostExecute(students);
            //dialog.dismiss();
            listAdapter.notifyDataSetChanged();
            gridAdapter.notifyDataSetChanged();

        }
    }
}

