package com.student.click.entities;

import java.io.Serializable;

public class Student implements Serializable {
    String name;
    String roll;
    String branch;
    String image;

    Student() {
    }

    public Student(String name, String roll, String branch, String image) {
        this.name = name;
        this.roll = roll;
        this.branch = branch;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}