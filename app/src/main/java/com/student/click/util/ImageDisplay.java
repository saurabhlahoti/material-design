package com.student.click.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import com.student.click.studentmanagementsystem.R;

import java.io.File;
import java.io.IOException;

public class ImageDisplay {

    public int profileImage() {
        return R.mipmap.profile;
    }

    public Bitmap renderImage(String imageName, Context ctx) {
        Bitmap bitmap = null;
       // String imagePath = Environment.getExternalStorageDirectory() + File.separator + "StudentMS" + File.separator + "SAVED" + File.separator;
        File imageFile = new File(Constant.IMAGE_DIRECTORY + File.separator + imageName);
        try {
            bitmap = MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), Uri.fromFile(imageFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
