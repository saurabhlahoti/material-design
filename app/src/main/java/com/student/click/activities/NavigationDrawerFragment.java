package com.student.click.activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.student.click.entities.DrawerItem;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.RecyclerViewAdapter;

import java.util.ArrayList;

public class NavigationDrawerFragment extends Fragment implements RecyclerViewAdapter.ListItemClickListener {

    ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawerLayout;
    boolean drawerViewed;
    boolean fromSavedState;
    View primaryView;
    View fragView;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<DrawerItem> itemList;
    RecyclerViewAdapter.ListItemClickListener listItemClickListener;
    int selectedListItem = -1;
    public NavigationDrawerFragment() {    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (savedInstanceState != null) {
            fromSavedState = savedInstanceState.getBoolean("fromSavedState");
            drawerViewed = savedInstanceState.getBoolean("drawerViewed");

        } else {
            drawerViewed = false;
            fromSavedState = false;
        }

        itemList = new ArrayList<>();
        primaryView = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView) primaryView.findViewById(R.id.drawerRecycler);
        itemList.add(new DrawerItem(R.mipmap.user, "Async Task"));
        itemList.add(new DrawerItem(R.mipmap.bin, "Services"));
        itemList.add(new DrawerItem(R.mipmap.clear, "Intent Services "));
        recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), itemList);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.setListItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        registerForContextMenu(recyclerView);
        return primaryView;
    }


    public void setUp(int fragId, DrawerLayout layout, final Toolbar toolbar) {
        drawerLayout = layout;
        fragView = getActivity().findViewById(fragId);
        toolbar.setAlpha(1);
        actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                if (drawerViewed == false) {
                    drawerViewed = true;
                }
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                //toolbar.setAlpha(1 - (slideOffset / 2));
            }
        };
        if (drawerViewed == false && fromSavedState == false) {
            //drawerLayout.openDrawer(fragView);
            drawerViewed = true;
        }
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("drawerViewed", drawerViewed);
        fromSavedState = true;
        outState.putBoolean("rotation", fromSavedState);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        String[] menuItems = getResources().getStringArray(R.array.menu);
        for (int i = 0; i < menuItems.length; i++) {
            menu.add(Menu.NONE, i, i, menuItems[i]);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Toast.makeText(getActivity(), "option cl icked @ " + item.getItemId(), Toast.LENGTH_SHORT).show();
        Toast.makeText(getActivity(), "Item clicked @ " + selectedListItem, Toast.LENGTH_SHORT).show();
        return super.onContextItemSelected(item);

    }

    @Override
    public void listItemLongClicked(View view, int position) {
        Toast.makeText(getActivity(), "Item click @ " + position, Toast.LENGTH_SHORT).show();
        selectedListItem = position;
    }
}
