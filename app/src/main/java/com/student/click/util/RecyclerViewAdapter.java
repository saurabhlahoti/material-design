package com.student.click.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.student.click.entities.DrawerItem;
import com.student.click.studentmanagementsystem.R;

import java.util.ArrayList;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyHolder> {


    private LayoutInflater inflater;
    ArrayList<DrawerItem> drawerList;
    Context context;
    ListItemClickListener listItemClickListener;
    public RecyclerViewAdapter(Context context, ArrayList<DrawerItem> list) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        drawerList = list;
    }

    public void setListItemClickListener(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    @Override
    public RecyclerViewAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.recycler_view_structure, parent, false);
        MyHolder myHolder = new MyHolder(view);
        Log.d("check","called adapter");
        return myHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.MyHolder myHolder,final int position) {
        DrawerItem item = drawerList.get(position);
        Log.d("check", "called for:" + position);
        myHolder.textView.setText(item.getItemName());
        myHolder.imageView.setImageResource(item.getImageId());
/*
        1st way of setting onCLickListener on item
        myHolder.imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Item click @ " + position, Toast.LENGTH_SHORT).show();
            }
        });
        myHolder.textView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Item click @ " + position, Toast.LENGTH_SHORT).show();
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return drawerList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener,View.OnClickListener {
        ImageView imageView;
        TextView textView;

        public MyHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.studentImageRV);
            textView = (TextView) itemView.findViewById(R.id.studentNameRV);
            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public boolean onLongClick(View view){
            if(listItemClickListener!=null) {
                listItemClickListener.listItemLongClicked(view, getPosition());
            }
            return false;
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, "Item click @ " + getPosition(), Toast.LENGTH_SHORT).show();
            deleteItem(getPosition());
        }
    }

    void deleteItem(int position){
        //drawerList.remove(position);
        //notifyItemRemoved(position);
    }


    public interface ListItemClickListener{
        void listItemLongClicked(View view,int position);
    }
}
