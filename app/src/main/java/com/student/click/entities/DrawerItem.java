package com.student.click.entities;


public class DrawerItem {
    int imageId;
    String itemName;

    public DrawerItem(int imageId, String itemName) {
        this.imageId = imageId;
        this.itemName = itemName;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
